package com.techu.team4.api.repository;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClienteRepositoryImpl implements ClienteRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public ClienteRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Cliente> findAll() {
        List<Cliente> clientes = this.mongoOperations.find(new Query(), Cliente.class);
        return clientes;
    }

    @Override
    public Cliente findOne(String id) {
        Cliente encontrado = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)),Cliente.class);
        return encontrado;
    }

    @Override
    public Cliente saveCliente(Cliente client) {
        this.mongoOperations.save(client);
        return findOne(client.getId());
    }

    @Override
    public void updateCliente(Cliente client) {
        this.mongoOperations.save(client);
    }

    @Override
    public void deleteCliente(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), Cliente.class);
    }

    @Override
    public List<Cliente> findByFilters(String idDocument, String typeDocument, String state) {

        Criteria criteria = new Criteria();
        criteria.and("idDocument").is(idDocument);
        criteria.and("typeDocument").is(typeDocument);
        criteria.and("state").is(state);

        List<Cliente> tarjetas = this.mongoOperations.find(new Query( criteria) , Cliente.class);
        return tarjetas;
    }

    @Override
    public Cliente findByFiltersLogin(String idDocument, String typeDocument, String pass) {

        Criteria criteria = new Criteria();
        criteria.and("idDocument").is(idDocument);
        criteria.and("typeDocument").is(typeDocument);
        criteria.and("password").is(pass);

        Cliente tarjetas = this.mongoOperations.findOne(new Query( criteria) , Cliente.class);
        return tarjetas;
    }
}
