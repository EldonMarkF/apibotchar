package com.techu.team4.api.repository;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import org.apache.juli.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetasRepositoryImpl  implements TarjetasRepository{

    private final MongoOperations mongoOperations;
    private static final Logger LOGGER = LoggerFactory.getLogger(TarjetasRepositoryImpl.class);

    @Autowired
    public TarjetasRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Tarjetas> findAll() {
        List<Tarjetas> tarjetas = this.mongoOperations.find(new Query(), Tarjetas.class);
        return tarjetas;
    }

    @Override
    public List<Tarjetas> findByFilters(String customerId,String cardType,String brand) {
        LOGGER.info(customerId,cardType,brand);
        LOGGER.info(cardType);
        LOGGER.info(brand);

        Criteria criteria = new Criteria();
        criteria.and("idClient").is(customerId);
        criteria.and("typeCard").is(cardType);
        criteria.and("brand").is(brand);

        List<Tarjetas> tarjetas = this.mongoOperations.find(new Query( criteria) , Tarjetas.class);
        LOGGER.info(tarjetas.toString());
        return tarjetas;
    }

    @Override
    public Tarjetas findOne(String id) {
        Tarjetas encontrado = this.mongoOperations.findOne(new Query(Criteria.where("_id").is(id)),Tarjetas.class);
        return encontrado;
    }

    @Override
    public Tarjetas saveTarjetas(Tarjetas tarjetas) {
        this.mongoOperations.save(tarjetas);
        return findOne(tarjetas.getId());
    }

    @Override
    public void updateTarjetas(Tarjetas client) {
        this.mongoOperations.save(client);
    }

    @Override
    public void deleteTarjetas(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), Tarjetas.class);
    }
}
