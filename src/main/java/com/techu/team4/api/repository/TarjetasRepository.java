package com.techu.team4.api.repository;

import com.techu.team4.api.models.Tarjetas;

import java.util.List;

public interface TarjetasRepository {

    List<Tarjetas> findAll();
    public Tarjetas findOne(String id);
    public Tarjetas saveTarjetas(Tarjetas tarjetas);
    public void updateTarjetas(Tarjetas tarjetas);

    public void deleteTarjetas(String id);

    public List<Tarjetas> findByFilters(String customerId,String cardType,String brand) ;
}
