package com.techu.team4.api.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document(collection = "tarjetas")
@JsonPropertyOrder({"id","idClient", "cardNumber", "contractNumber","dateOrigin", "brand","typeCard", "statusCard"})
public class Tarjetas implements Serializable {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String idClient;
    @NotNull
    private String contractNumber;
    @NotNull
    private String cardNumber;
    @NotNull
    private String brand;
    @NotNull
    private String typeCard;
    @NotNull
    private String statusCard;
    @NotNull
    private Date dateOrigin;

    public Tarjetas(){

    }

    public Tarjetas(@NotNull String id, @NotNull String contractNumber, @NotNull String cardNumber, @NotNull String brand, @NotNull String typeCard, @NotNull String statusCard, @NotNull Date dateOrigin, @NotNull Date dateModify) {
        this.idClient = id;
        this.contractNumber = contractNumber;
        this.cardNumber = cardNumber;
        this.brand = brand;
        this.typeCard = typeCard;
        this.statusCard = statusCard;
        this.dateOrigin = dateOrigin;
        this.dateModify = dateModify;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getId() {
        return id;
    }

    public void setId(String idClient) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTypeCard() {
        return typeCard;
    }

    public void setTypeCard(String typeCard) {
        this.typeCard = typeCard;
    }

    public String getStatusCard() {
        return statusCard;
    }

    public void setStatusCard(String statusCard) {
        this.statusCard = statusCard;
    }

    public Date getDateOrigin() {
        return dateOrigin;
    }

    public void setDateOrigin(Date dateOrigin) {
        this.dateOrigin = dateOrigin;
    }

    public Date getDateModify() {
        return dateModify;
    }

    public void setDateModify(Date dateModify) {
        this.dateModify = dateModify;
    }

    @NotNull
    private Date dateModify;


}
