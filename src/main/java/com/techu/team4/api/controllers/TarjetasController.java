package com.techu.team4.api.controllers;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import com.techu.team4.api.service.ClienteService;
import com.techu.team4.api.service.TarjetasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("tarjetas")
public class TarjetasController {

    private final TarjetasService tarjetasService;
    private Tarjetas tarjetas;

    @Autowired
    public TarjetasController(TarjetasService tarjetasService)
    {
        this.tarjetasService = tarjetasService;
    }

    @GetMapping()
    public ResponseEntity<List<Tarjetas>> tarjetas() {
        System.out.println("Me piden la lista de tarjetas");
        return ResponseEntity.ok(tarjetasService.findAll());
    }

    @GetMapping("/{customerId}/{cardType}/{brand}")
    public ResponseEntity<List<Tarjetas>> tarjetas(@PathVariable String customerId,
                                                   @PathVariable String cardType,
                                                   @PathVariable String brand) {
        System.out.println("Me piden la lista de tarjetas");
        return ResponseEntity.ok(tarjetasService.findByFilters( customerId, cardType, brand));
    }


    @PostMapping()
    public ResponseEntity<Tarjetas> saveTarjetas(@RequestBody Tarjetas tarjetas)
    {
        return ResponseEntity.ok(tarjetasService.saveTarjetas(tarjetas));
    }

    @PutMapping("/{id}/{operacion}")
    public ResponseEntity<Tarjetas> updateTarjetas(@PathVariable String id,
                                                   @PathVariable String operacion)
    {
        return ResponseEntity.ok(tarjetasService.blockUnblockCard(id,operacion));
    }
}
