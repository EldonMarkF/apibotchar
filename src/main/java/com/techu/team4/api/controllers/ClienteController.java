package com.techu.team4.api.controllers;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import com.techu.team4.api.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("clientes")
public class ClienteController {
    private final ClienteService clienteService;
    private Cliente cliente;

    @Autowired
    public ClienteController(ClienteService clienteService)
    {
        this.clienteService = clienteService;
    }

    @GetMapping()
    public ResponseEntity<List<Cliente>> clientes() {
        System.out.println("Me piden la lista de clientes");
        return ResponseEntity.ok(clienteService.findAll());
    }

    @GetMapping("/{idDocument}/{typeDocument}/{state}")
    public ResponseEntity<List<Cliente>> clientes(@PathVariable String idDocument,
                                                   @PathVariable String typeDocument,
                                                   @PathVariable String state) {
        return ResponseEntity.ok(clienteService.findByFilters( idDocument, typeDocument, state));
    }

  /*  @PostMapping()
    public ResponseEntity<Cliente> saveVehiculo(@RequestBody Cliente cliente)
    {
        return ResponseEntity.ok(clienteService.saveCliente(cliente));
    }*/

    @PostMapping()
    public ResponseEntity<Cliente> loginUSer(@RequestBody Cliente cliente)
    {
        return ResponseEntity.ok(
                clienteService.findByFiltersLogin(cliente.getIdDocument(),cliente.getTypeDocument(),cliente.getPassword()));
    }

}
