package com.techu.team4.api.service;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;

import java.util.List;

public interface ClienteService {
    List<Cliente> findAll();
    public Cliente findOne(String id);
    public Cliente saveCliente(Cliente veh);
    public void updateCliente(Cliente veh);
    public void deleteCliente(String id);
    public List<Cliente> findByFilters(String idDocument, String typeDocument, String state) ;
    public Cliente findByFiltersLogin(String idDocument, String typeDocument, String pass) ;
}
