package com.techu.team4.api.service;

import com.techu.team4.api.models.Tarjetas;

import java.util.List;

public interface TarjetasService {

    List<Tarjetas> findAll();
    public Tarjetas findOne(String id);
    public Tarjetas saveTarjetas(Tarjetas veh);
    public void updateTarjetas(Tarjetas veh);
    public void deleteTarjetas(String id);
    public Tarjetas blockUnblockCard(String id,String operacion);
    public List<Tarjetas> findByFilters(String customerId,String cardType,String brand) ;
}
