package com.techu.team4.api.service;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import com.techu.team4.api.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("clienteService")
@Transactional
public class ClienteServiceImpl implements ClienteService {

    private ClienteRepository clienteRepository;

    @Autowired
    public ClienteServiceImpl(ClienteRepository clienteRepository)
    {
        this.clienteRepository = clienteRepository;
    }

    @Override
    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente findOne(String id) {
        return clienteRepository.findOne(id);
    }

    @Override
    public Cliente saveCliente(Cliente veh) {
        return clienteRepository.saveCliente(veh);
    }

    @Override
    public void updateCliente(Cliente veh) {
        clienteRepository.updateCliente(veh);
    }

    @Override
    public void deleteCliente(String id) {
        clienteRepository.deleteCliente(id);
    }

    @Override
    public List<Cliente> findByFilters(String idDocument, String typeDocument, String state) {
        return clienteRepository.findByFilters(idDocument,typeDocument,state);
    }
    @Override
    public Cliente findByFiltersLogin(String idDocument, String typeDocument, String pass) {
        return clienteRepository.findByFiltersLogin(idDocument,typeDocument,pass);
    }
}
