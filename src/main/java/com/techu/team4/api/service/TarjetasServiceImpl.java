package com.techu.team4.api.service;

import com.techu.team4.api.models.Cliente;
import com.techu.team4.api.models.Tarjetas;
import com.techu.team4.api.repository.ClienteRepository;
import com.techu.team4.api.repository.TarjetasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service("tarjetasService")
@Transactional
public class TarjetasServiceImpl  implements  TarjetasService{


    private TarjetasRepository tarjetasRepository;

    @Autowired
    public TarjetasServiceImpl(TarjetasRepository tarjetasRepository)
    {
        this.tarjetasRepository = tarjetasRepository;
    }

    @Override
    public List<Tarjetas> findAll() {
        return tarjetasRepository.findAll();
    }

    @Override
    public Tarjetas findOne(String id) {
        return tarjetasRepository.findOne(id);
    }

    @Override
    public List<Tarjetas> findByFilters(String customerId,String cardType,String brand) {
        return tarjetasRepository.findByFilters(customerId,cardType,brand);
    }

    @Override
    public Tarjetas saveTarjetas(Tarjetas veh) {
        return tarjetasRepository.saveTarjetas(veh);
    }

    @Override
    public void updateTarjetas(Tarjetas veh) {
        tarjetasRepository.updateTarjetas(veh);
    }

    @Override
    public void deleteTarjetas(String id) {
        tarjetasRepository.deleteTarjetas(id);
    }

    @Override
    public Tarjetas blockUnblockCard(String id, String operacion) {
        Tarjetas tarjetas= tarjetasRepository.findOne(id);
        tarjetas.setStatusCard(operacion);
        tarjetas.setDateModify(new Date());
        tarjetasRepository.updateTarjetas(tarjetas);
        return tarjetas;

    }

}
