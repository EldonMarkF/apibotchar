package com.techu.team4.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techu.team4.api.models.Cliente;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteServiceImplTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void GetClientesOK() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/clientes"))
                .andExpect(status().isOk());
    }

    @Test
    public void GetClienteInexistente() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/yyyyyyyy")).
                andExpect(status().isNotFound());
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void createCliente() throws Exception {
        /*String uri = "/clientes";
        Cliente cliente = new Cliente();
        cliente.setNombres("Mila");
        cliente.setApellidos("Manrique");

        String inputJson = mapToJson(cliente);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);*/
    }
}
